﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Universalis.Editors
{
    using Objective;

    [CustomEditor(typeof(LevelGenerator))]
    public class LevelEditor : Editor
    {
        LevelGenerator generator;

        public override void OnInspectorGUI()
        {
            generator = (LevelGenerator)target;
            Level level = generator.currentLevel;

            base.OnInspectorGUI();

            EditorGUILayout.Space();
            EditorGUILayout.LabelField("Level Design", EditorStyles.boldLabel);
            GUILayout.BeginVertical();
            {
                GUILayout.BeginHorizontal();
                {
                    if (GUILayout.Button("Add Orbit"))
                    {
                        Objective newObjective = new Objective();
                        newObjective.Points = new List<Vector3>(generator.bodyGenerator.GetLastTrajectory());
                        newObjective.CreateEllipse(Objective.DEFAULT_VERTICES);

                        if (newObjective.IsStable())
                        {
                            level.AddObjective(newObjective);
                        }
                        else
                        {
                            Debug.LogWarning("Current orbit is not stable. Cannot add objective.");
                        }

                        EditorUtility.SetDirty(generator);
                    }

                    if (GUILayout.Button("Redraw"))
                    {
                        level.Redraw();
                    }
                }
                GUILayout.EndHorizontal();

                if (GUILayout.Button("Clear All"))
                {
                    string confirm = "Are you sure you want to clear all level data? This cannot be undone.";
                    if (true == EditorUtility.DisplayDialog("Confirm", confirm, "Yes", "No"))
                    {
                        level.Clear();
                        EditorUtility.SetDirty(generator);
                    }
                }
            }
            GUILayout.EndVertical();

            EditorGUILayout.Space();
            EditorGUILayout.LabelField("File Management", EditorStyles.boldLabel);
            GUILayout.BeginVertical();
            {
                GUILayout.BeginHorizontal();
                {
                    if (GUILayout.Button("Save Data"))
                    {
                        bool doSave = true;

                        if (LevelIO.Exists(level.LevelNumber))
                        {
                            string confirm = "Are you sure you want to overwrite " + level + " data?";
                            doSave = EditorUtility.DisplayDialog("Confirm Overwrite", confirm, "Yes", "No");
                        }

                        if (doSave)
                        {
                            string data = LevelIO.Save(level);
                            LevelIO.WriteToFile(level.LevelNumber, data);
                        }
                    }

                    if (GUILayout.Button("Load Data"))
                    {
                        Debug.Log("Loading data...");

                        string data = LevelIO.ReadFromFile(level.LevelNumber);
                        level.Data = LevelIO.Load(data);
                    }
                }
                GUILayout.EndHorizontal();

            }
            GUILayout.EndVertical();
        }
    }
}
