﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Universalis.Editors
{
    [CustomEditor(typeof(SolarSystem))]
    public class SolarSystemEditor : Editor
    {
        SolarSystem system;

        public override void OnInspectorGUI()
        {
            system = (SolarSystem)target;

            base.OnInspectorGUI();

            if (GUILayout.Button("Reset System"))
            {
                string confirm = "Are you sure you want to reset the system? You cannot undo this action.";
                if (true == EditorUtility.DisplayDialog("Confirm Reset", confirm, "Yes", "Cancel"))
                {
                    system.Reset();
                }
            }
        }
    }
}
