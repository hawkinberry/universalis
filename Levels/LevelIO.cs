﻿using System.IO;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

namespace Universalis.Editors
{
    using Objective;

    public static class LevelIO
    {
        private static string FILEPATH = "Assets/Resources/Levels/";
        private static string TYPE = ".json";

        public static bool Exists(int lvl)
        {
            string filename = FileName(lvl);

            return File.Exists(filename);
        }

        private static string FileName(int lvl)
        {
            return (FILEPATH + lvl + TYPE);
        }

        #region Serialization
        private static void Serialize(Stream stream, object o)
        {
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(stream, o);
        }

        public static string Deserialize(Stream stream)
        {
            BinaryFormatter bf = new BinaryFormatter();
            return bf.Deserialize(stream).ToString();
        }

        public static string Save(Level level)
        {
            return JsonUtility.ToJson(level.Data, true);
        }

        public static LevelData Load(string contents)
        {
            LevelData data = new LevelData();
            JsonUtility.FromJsonOverwrite(contents, data);

            return data;
        }
        #endregion

        #region File I/O
        public static string ReadFromFile(int lvl)
        {
            string filename = FileName(lvl);
            string data = "";

            if (File.Exists(filename))
            {
                //FileStream file = File.OpenRead(filename);
                //data = Deserialize(file);
                //file.Close();

                byte[] buffer = File.ReadAllBytes(filename);
                data = Encoding.UTF8.GetString(buffer, 0, buffer.Length);

            }

            return data;
        }

        private static void SetReadOnly(string filename, bool setting)
        {
            FileInfo fInfo = new FileInfo(filename);
            fInfo.IsReadOnly = setting;
        }

        public static void WriteToFile(int lvl, string data)
        {
            string filename = FileName(lvl);

            if (File.Exists(filename))
            {
                SetReadOnly(filename, false);
            }

            FileStream file = File.Open(filename, FileMode.OpenOrCreate);
            //StreamWriter stream = new StreamWriter(file, System.Text.Encoding.GetEncoding("UTF-8"));
            //Serialize(stream.BaseStream, data);
            //Serialize(file, data);
            byte[] buffer = Encoding.UTF8.GetBytes(data);
            file.Write(buffer, 0, buffer.Length);
            file.Close();

            // Set file read-only, as editing can corrupt format
            SetReadOnly(filename, true);
        }
        #endregion
    }
}
