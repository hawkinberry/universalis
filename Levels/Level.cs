﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Universalis.Objective
{
    using UMath;

    public class Level : MonoBehaviour
    {
        #region Delegates
        public static event LevelLoadHandler OnLoad;
        public delegate void LevelLoadHandler();

        public static event ObjectiveCompleteHandler OnObjectiveComplete;
        public delegate void ObjectiveCompleteHandler();

        public static event LevelCompleteHandler OnLevelComplete;
        public delegate void LevelCompleteHandler();
        #endregion

        public GameObject renderTemplate;
        public SolarSystem SolarSystem;

        public int LevelNumber
        {
            get { return data.levelNumber; }
        }

        [Header("Objectives")]
        public float Tolerance = 0.1f;

        // Level Data
        [SerializeField] private LevelData data = new LevelData();
        public LevelData Data
        {
            get { return data; }
            set
            {
                data = value;
                Debug.Log("Level " + LevelNumber + " loaded.");
                ResetLevel();
            }
        }

        public List<Objective> Objectives
        {
            get { return data.Objectives; }
        }

        public string Hint
        {
            get { return data.hintText; }
        }

        private List<ObjectiveRenderer> renderers = new List<ObjectiveRenderer>();

        public Dictionary<int, bool> ObjectiveStatus = new Dictionary<int, bool>();
        private bool m_isComplete = false;

        // Start is called before the first frame update
        void Start()
        {
            Data = LevelManager.GetLevelData(LevelManager.CurrentLevel);
            OnLoad?.Invoke();
        }

        void Update()
        {
            // Check objectives
            string statusStr = "";
            for (int idx = 0; idx < Objectives.Count; idx++)
            {
                statusStr += String.Format("({0}) : {1}, ", idx, ObjectiveStatus[idx]);

                Objective o = Objectives[idx];

                //Debug.Log(o.ToString());
                bool oldState = ObjectiveStatus[idx];
                bool atLeastOne = false;

                foreach (Ellipse.Parameters p in SolarSystem.orbits)
                {
                    if (Ellipse.Equivalent(o.Form, p, Tolerance))
                    {
                        atLeastOne = true;
                    }
                }

                if (atLeastOne == true)
                {
                    ObjectiveStatus[idx] = true;
                    renderers[idx].Complete();
                    OnObjectiveComplete?.Invoke();
                }
                else
                {
                    ObjectiveStatus[idx] = false;
                    renderers[idx].ResetColor();
                }
            }

            //Debug.Log("Status: " + statusStr);

            if (IsLevelComplete())
            {
                if (false == m_isComplete)
                {
                    Debug.Log("Level complete!");
                    OnLevelComplete?.Invoke();
                }

                m_isComplete = true;
            }
        }

        bool IsLevelComplete()
        {
            bool complete = true;

            foreach (bool b in ObjectiveStatus.Values)
            {
                complete &= b;
            }

            return complete;
        }

        void Draw()
        {
            foreach (Objective o in Objectives)
            {
                GameObject render = GameObject.Instantiate(renderTemplate, transform);
                ObjectiveRenderer or = render.GetComponent<ObjectiveRenderer>();

                or.Objective = o;
                or.Draw();
                renderers.Add(or);
            }
        }

        #region Manage Objectives
        void OnValidate()
        {
            // In case the form of an objective has been changed, the points need to be updated
            foreach (Objective o in Objectives)
            {
                o.CreateEllipse(Objective.DEFAULT_VERTICES);
            }
        }

        public void AddObjective(Objective objective)
        {
            Objectives.Add(objective);
        }

        public void Clear()
        {
            if (data.Objectives != null)
            {
                Objectives.Clear();
            }
        }

        public void Redraw()
        {
            foreach (ObjectiveRenderer or in renderers)
            {
                Destroy(or.gameObject);
            }
            renderers.Clear();

            Draw();
        }

        public void ResetLevel()
        {
            LevelManager.CurrentLevel = this.LevelNumber;

            ObjectiveStatus.Clear();

            for (int idx = 0; idx < Objectives.Count; idx++)
            {
                ObjectiveStatus.Add(idx, false);
            }

            Redraw();
        }
        #endregion

        public override string ToString()
        {
            return "Level " + LevelNumber;
        }
    }
}
