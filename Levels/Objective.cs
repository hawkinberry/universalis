﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Universalis.Objective
{
    using UMath;

    [Serializable]
    public class Objective
    {
        enum Rotation
        {
            COUNTER_CLOCKWISE = -1,
            EITHER = 0,
            CLOCKWISE = 1
        };

        public static int DEFAULT_VERTICES = 5000;

        [SerializeField] private Ellipse.Parameters m_form = new Ellipse.Parameters { };
        [SerializeField] private List<Vector3> m_points = new List<Vector3>();

        [SerializeField] private int m_multiplicty = 1;
        [SerializeField] private Rotation m_rotation = Rotation.EITHER;

        public List<Vector3> Points
        {
            get { return m_points; }
            set
            {
                m_points = value;
                m_form = Ellipse.Fit(m_points.ToArray());
            }
        }

        public Ellipse.Parameters Form
        {
            get { return m_form; }
            protected set { m_form = value; }
        }

        public Vector3[] GetArray()
        {
            return Points.ToArray();
        }

        public void CreateEllipse(int aResolution)
        {
            Vector3[] positions = Ellipse.CreatePoints(m_form, aResolution);
            m_points = new List<Vector3>(positions);
        }

        public bool IsStable()
        {
            return Ellipse.IsWellFormed(m_form);
        }

        public override string ToString()
        {
            return m_form.ToString();
        }
    }
}