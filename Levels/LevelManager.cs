﻿using UnityEngine;

namespace Universalis.Objective
{
    public static class LevelManager
    {
        private static string FILEPATH = "Levels/";

        public static int CurrentStage = 1;
        public static int CurrentLevel = 1;

        public static bool Exists(int lvl)
        {
            string filename = FileName(lvl);

            TextAsset data = Resources.Load<TextAsset>(filename);

            return (data != null);
        }

        public static LevelData GetLevelData(int lvl)
        {
            string filename = FileName(lvl);
            string contents = "";

            Debug.Log("Getting level data for " + filename);
            var file = Resources.Load<TextAsset>(filename);

            if (file) { contents = file.text; }
            else { Debug.LogError("File does not exist"); }

            LevelData data = Editors.LevelIO.Load(contents);

            return data;
        }

        private static string FileName(int lvl)
        {
            return (FILEPATH + lvl);
        }
    }
}