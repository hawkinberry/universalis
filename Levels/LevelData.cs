﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Universalis.Objective
{
    [Serializable]
    public class LevelData
    {
        [SerializeField] public int levelNumber = 0;
        [SerializeField] public string hintText = "";

        [SerializeField] private List<Objective> m_objectives = new List<Objective>();

        public List<Objective> Objectives
        {
            get { return m_objectives; }
            set { m_objectives = value; }
        }
}
}
