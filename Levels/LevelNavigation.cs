﻿using System.Collections;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

namespace Universalis.Objective
{
    public class LevelNavigation : MonoBehaviour
    {
        [Header("Navigation")]
        public TextMeshProUGUI winText;
        public GameObject navigation;
        public GameObject nextButton;
        public TextMeshProUGUI comeBack;

        [Header("Current Level")]
        public Level m_level;
        public TextMeshProUGUI levelNumber;
        public TextMeshProUGUI hint;

        void Start()
        {
            hint.enabled = true;
            winText.enabled = false;
            comeBack.enabled = false;

            navigation.SetActive(false);

            levelNumber.text = LevelManager.CurrentLevel.ToString();
        }

        // Start is called before the first frame update
        void OnEnable()
        {
            Level.OnLoad += LoadComplete;
            Level.OnLevelComplete += ShowLevelComplete;
        }

        void OnDisable()
        {
            Level.OnLoad -= LoadComplete;
            Level.OnLevelComplete -= ShowLevelComplete;
        }

        void LoadComplete()
        {
            hint.text = m_level.Hint;
        }

        void ShowLevelComplete()
        {
            hint.enabled = false;
            winText.enabled = true;

            navigation.SetActive(true);
            int nextLevel = LevelManager.CurrentLevel + 1;
            if (LevelManager.Exists(nextLevel))
            {
                nextButton.SetActive(true);
            }
            else
            {
                nextButton.SetActive(false);
                comeBack.enabled = true;
            }
        }

        public static void LoadLevel()
        {
            SceneManager.LoadScene("ObjectiveMode");
        }

        public static void LoadLevelSelection()
        {
            // placeholder
            LoadLevel();
        }

        public static AsyncOperation LoadLevelAsync()
        {
            return SceneManager.LoadSceneAsync("ObjectiveMode");
        }

        public static AsyncOperation LoadLevelSelectionAsync()
        {
            // placeholder
            return LoadLevelAsync();
        }

        public void LoadNextLevel()
        {
            int nextLevel = LevelManager.CurrentLevel + 1;
            Debug.Log("Loading next level ...");
            if (LevelManager.Exists(nextLevel))
            {
                LevelManager.CurrentLevel = nextLevel;
                //m_level.Load(LevelManager.GetLevelData(nextLevel));
                LoadLevelAsync();
            }
        }
    }
}
