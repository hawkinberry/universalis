﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Universalis.Objective
{
    [RequireComponent(typeof(LineRenderer))]
    public class ObjectiveRenderer : MonoBehaviour
    {
        public Color defaultColor;
        public Color completeColor;
        private Color highlightColor;

        private LineRenderer lineRenderer;

        public Objective Objective { get; set; }

        void Awake()
        {
            lineRenderer = transform.GetComponent<LineRenderer>();
        }

        public void Draw()
        {
            lineRenderer.positionCount = Objective.Points.Count;
            lineRenderer.SetPositions(Objective.Points.ToArray());
        }

        public void ResetColor()
        {
            lineRenderer.startColor = defaultColor;
            lineRenderer.endColor   = defaultColor;
        }

        public void Complete()
        {
            lineRenderer.startColor = completeColor;
            lineRenderer.endColor   = completeColor;
        }
    }
}
