﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Universalis
{
    namespace Audio
    {
        [RequireComponent(typeof(AudioSource))]
        // Based on https://www.youtube.com/watch?v=8pFlnyfRfRc
        public class AudioManager : MonoBehaviour
        {
            static bool m_Muted = false;
            public static bool IsMuted { get { return m_Muted; } }

            static public AudioManager m_Instance;

            // Audio clip collections
            static List<AudioClip> m_sounds = new List<AudioClip>();

            // Single shots
            static AudioClip m_Button;
            static AudioClip m_Explosion;

            // Music
            public GameTrack trackPlayer;

            // Audio sources
            private static AudioSource m_SfxSource;
            private static AudioSource m_MusicPlayer;

            // Audio source collections
            private static List<AudioSource> m_Sources;
            private static List<AudioSource> m_SfxSources;
            private static List<AudioSource> m_MusicSources;

            // Configurable volume settings
            public static float VOLUME = 1.0f;
            public static float SFX_VOLUME = 1.0f;
            public static float MUSIC_VOLUME = 1.0f;

            // Volume profiles
            private const float MUSIC_BALANCE = 0.5f;

            public static void RegisterMusicSource(ref AudioSource aSource)
            {
                m_MusicSources.Add(aSource);
                m_Sources.Add(aSource);
            }

            private static void SetVolume(ref List<AudioSource> rSources, float aSetting)
            {
                foreach (AudioSource src in rSources)
                {
                    src.volume = aSetting;
                }
            }

            public static void SetAllVolume(float input)
            {
                VOLUME = input;
                SetVolume(ref m_Sources, VOLUME);
            }

            public static void SetSfxVolume(float input)
            {
                SFX_VOLUME = input;
                SetVolume(ref m_SfxSources, Mathf.Min(VOLUME, SFX_VOLUME));
            }

            public static void SetMusicVolume(float input)
            {
                MUSIC_VOLUME = input;
                SetVolume(ref m_MusicSources, Mathf.Min(VOLUME, MUSIC_VOLUME));
            }

            void Awake()
            {
                m_Instance = this;

                m_SfxSource = GetComponent<AudioSource>();
                m_MusicPlayer = gameObject.transform.Find("Music").GetComponent<AudioSource>();

                // I would like a more modular way to do this
                m_Sources = new List<AudioSource> { m_SfxSource, m_MusicPlayer };
                m_SfxSources = new List<AudioSource> { m_SfxSource };
                m_MusicSources = new List<AudioSource> { m_MusicPlayer };
            }

            // Start is called before the first frame update
            void Start()
            {
                // Load samples
                m_Button = Resources.Load<AudioClip>("Sounds/buttonSelect");
                m_Explosion = Resources.Load<AudioClip>("Sounds/distant-explosion");

                m_MusicPlayer.enabled = true;
                m_MusicPlayer.volume = 0f;
                m_MusicPlayer.Play();
                StartCoroutine(StartFade(m_MusicPlayer, 10f, MUSIC_BALANCE));
            }

            // See https://gamedevbeginner.com/how-to-fade-audio-in-unity-i-tested-every-method-this-ones-the-best/
            public static IEnumerator StartFade(AudioSource audioSource, float duration, float targetVolume)
            {
                float currentTime = 0;
                float start = audioSource.volume;

                while (currentTime < duration)
                {
                    currentTime += Time.deltaTime;
                    audioSource.volume = Mathf.Lerp(start, targetVolume, currentTime / duration);
                    yield return null;
                }
                yield break;
            }

            public static bool ToggleMute()
            {
                SetMute(!m_Muted);
                return m_Muted;
            }

            public static void SetMute(bool setting)
            {
                m_Muted = setting;

                foreach (AudioSource s in m_Sources)
                {
                    s.mute = m_Muted;
                }

                GameTrack tp = m_Instance.trackPlayer;
                if (tp)
                {
                    if (m_Muted)
                    {
                        tp.DisablePlayback();
                    }
                    else
                    {
                        tp.EnablePlayback();
                    }
                }
            }

            public static void SetPause(bool pause)
            {
                foreach (AudioSource s in m_Sources)
                {
                    if (pause)
                    {
                        s.Pause();
                    }
                    else
                    {
                        s.UnPause();
                    }
                }
            }

            public static void PlaySound(string clip)
            {
                switch (clip)
                {
                    case "explosion":
                        PlayExplosion();
                        break;
                    case "button":
                        PlayButtonPress();
                        break;
                    default:
                        Debug.Log("Invalid audio clip " + clip + " requested");
                        break;
                }
            }

            private static AudioClip PlayRandomSound(ref AudioSource aSource, ref List<AudioClip> aList, float aVolume)
            {
                int choice = Random.Range(0, aList.Count);
                aSource.PlayOneShot(aList[choice], aVolume);

                return aList[choice];
            }

            private static void PlayExplosion()
            {
                m_SfxSource.PlayOneShot(m_Explosion, SFX_VOLUME);
            }

            private static void PlayButtonPress()
            {
                m_SfxSource.PlayOneShot(m_Button, SFX_VOLUME * 0.5f);
            }
        }
    }
}
