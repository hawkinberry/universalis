﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Universalis
{
    namespace Audio
    {
        public class GameTrack : MonoBehaviour
        {
            public AudioSource TrackTemplate;

            public List<AudioClip> m_TrackOrder = new List<AudioClip>();
            private List<AudioSource> m_Sources;
            private List<bool> m_Muted;

            void OnEnable()
            {
                Bodies.Body.OnDelete += MuteTrack;
            }

            void OnDisable()
            {
                Bodies.Body.OnDelete -= MuteTrack;
            }

            // Start is called before the first frame update
            void Start()
            {
                m_Sources = new List<AudioSource>();
                m_Muted = new List<bool>();

                foreach (AudioClip clip in m_TrackOrder)
                {
                    AudioSource track = GameObject.Instantiate(TrackTemplate, this.transform);
                    track.clip = clip;

                    m_Sources.Add(track);
                    m_Muted.Add(track.mute);
                    //Audio.AudioManager.RegisterMusicSource(ref track);
                }

                // Minimize overhead
                foreach (AudioSource track in m_Sources)
                {
                    track.Play();
                }

                // Start base track
                if (m_Sources.Count > 0)
                {
                    PlayTrack(0);
                }
            }

            #region Audio Management
            public void EnablePlayback()
            {
                for (int idx = 0; idx < m_Sources.Count; idx++)
                {
                    AudioSource source = m_Sources[idx];
                    if (m_Muted[idx] == false)
                    {
                        source.mute = false;
                    }
                }
            }

            public void DisablePlayback()
            {
                for (int idx = 0; idx < m_Sources.Count; idx++)
                {
                    AudioSource source = m_Sources[idx];
                    source.mute = true;
                }
            }
            #endregion

            #region Track Management
            private void PlayTrack(int idx)
            {
                if (idx >= 0 && idx < m_Sources.Count)
                {
                    m_Sources[idx].mute = AudioManager.IsMuted;
                    m_Muted[idx] = false;
                }
            }

            public int AddTrack()
            {
                for (int idx = 0; idx < m_Sources.Count; idx++)
                {
                    AudioSource source = m_Sources[idx];
                    if (m_Muted[idx] == true)
                    {
                        PlayTrack(idx);
                        return idx;
                    }
                }

                return -1;
            }

            private void MuteTrack(Bodies.Body body)
            {
                int idx = body.TrackID;

                // Never delete the base track (0)
                if (idx > 0 && idx < m_Sources.Count)
                {
                    if (m_Sources[idx] != null)
                    {
                        m_Sources[idx].mute = true;
                        m_Muted[idx] = true;
                    }
                    else
                    {
                        Debug.Log("Null audio source in track list. ID " + idx + " out of " + m_Sources.Count);
                    }
                }
            }
            #endregion
        }
    }
}