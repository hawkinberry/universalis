﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Universalis
{
    namespace Audio
    {
        public class TrackPlayer : MonoBehaviour
        {
            public AudioSource StartPlayer { get; private set; }
            public AudioSource LoopPlayer { get; private set; }

            // Start is called before the first frame update
            void Start()
            {
                StartPlayer = transform.Find("Start").GetComponent<AudioSource>();
                LoopPlayer = transform.Find("Loop").GetComponent<AudioSource>();

                Debug.Log("Starting main track");
                StartCoroutine(StartTrack());
            }

            private IEnumerator StartTrack()
            {
                StartPlayer.Play();
                yield return new WaitForSeconds(StartPlayer.clip.length - 0.3f);

                LoopPlayer.Play();
            }
        }
    }
}
