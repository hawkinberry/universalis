﻿using System;
using UnityEngine;
using Meta.Numerics.Matrices;
using Meta.Numerics;

namespace Universalis.UMath
{
    public class Circle
    {
        [Serializable]
        public struct Parameters
        {
            public float radius;
            public Vector2 center;
        }

        //! Determines whether an intersection occurs between point A and point B using a Ray2D on the indicated LAYER
        //! Returns true if intersection exists, false otherwise
        //! Out intersectionPoint: point of intersection, or Vector2.negativeInfinity if none exists
        public static bool DoesIntersectCircle(Vector2 pointA, Vector2 pointB, out Vector2 intersectionPoint, int LAYER=0)
        {
            bool intersection = false;
            intersectionPoint = Vector2.negativeInfinity;

            Vector2 castDirection = pointB - pointA;
            Ray2D ray = new Ray2D(pointA, castDirection);
            int layermask = (1 << LAYER);
            RaycastHit2D result = Physics2D.Raycast(pointA, castDirection, castDirection.magnitude, layermask);

            intersection = (result.collider != null);
            if (intersection)
            {
                intersectionPoint = result.point;
            }

            return intersection;
        }

        public static Vector3[] CreatePoints(Parameters param, int resolution)
        {
            return CreatePoints(param.radius, param.center, resolution);
        }

        public static Vector3[] CreatePoints(float r, Vector2 center, int resolution)
        {
            return Ellipse.CreatePoints(r, r, center, 0.0f, resolution);
        }
    }

    public class Ellipse
    {
        [Serializable]
        public struct Parameters
        {
            public float majorAxis;
            public float minorAxis;
            public Vector2 center;
            public float rotation;

            public override string ToString()
            {
                return String.Format("({0}, {1}) @ {2}; theta = {3})", majorAxis, minorAxis, center, rotation);
            }
        }

        public static bool IsWellFormed(Parameters param)
        {
            bool result = true;

            result &= !(param.majorAxis == Single.NaN);
            result &= !(param.minorAxis == Single.NaN);

            return result;
        }

        public static Vector3[] CreatePoints(Parameters param, int resolution)
        {
            return CreatePoints(param.majorAxis, param.minorAxis, param.center, param.rotation * Mathf.Rad2Deg, resolution);
        }

        public static float Eccentricity(Parameters param)
        {
            return Mathf.Sqrt(1 - Mathf.Pow(param.minorAxis, 2) / Mathf.Pow(param.majorAxis, 2));
        }

        private static float PerCentDifference(float A, float B)
        {
            return (A - B) / A;
        }

        private static float AbsoluteDifference(float A, float B)
        {
            return Mathf.Abs(A - B);
        }

        public static bool Equivalent(Parameters ellipse1, Parameters ellipse2, float tolerance = 0.01f)
        {
            bool equivalent = true;

            string deltaStr = "Deltas: ";

            // Compare axes
            float majorDelta = AbsoluteDifference(ellipse1.majorAxis, ellipse2.majorAxis);
            float minorDelta = AbsoluteDifference(ellipse1.minorAxis, ellipse2.minorAxis);
            deltaStr += String.Format("{0} (major); {1} (minor); ", majorDelta, minorDelta);
            equivalent &= (majorDelta <=  tolerance);
            equivalent &= (minorDelta <= tolerance);

            // Compare center
            float centerDelta = AbsoluteDifference(ellipse1.center.magnitude, ellipse2.center.magnitude);
            deltaStr += String.Format("{0} (center); ", centerDelta);
            equivalent &= (centerDelta <= tolerance);

            // Compare rotation - not for circles
            if (Eccentricity(ellipse1) > 0.01f && Eccentricity(ellipse2) > 0.01f)
            {
                // For small angles, if both are sufficiently small, consider it a pass
                if (Mathf.Abs(ellipse1.rotation) > tolerance && Mathf.Abs(ellipse2.rotation) > tolerance)
                {
                    float rotationDelta = AbsoluteDifference(ellipse1.rotation, ellipse2.rotation);

                    equivalent &= (rotationDelta <= tolerance);
                    deltaStr += String.Format("{0} (theta deg); ", rotationDelta);
                }
            }

            //Debug.Log(deltaStr);

            return equivalent;
        }

        // see https://answers.unity.com/questions/631201/draw-an-ellipse-in-unity-3d.html
        public static Vector3[] CreatePoints(float a, float b, Vector2 center, float thetaDeg, int resolution)
        {
            Vector3[] positions;
            float h = center.x;
            float k = center.y;

            positions = new Vector3[resolution + 1];
            Quaternion q = Quaternion.AngleAxis(thetaDeg, Vector3.forward);
            Vector3 center3D = new Vector3(h, k, Bodies.Body.Z_LOCK);

            for (int i = 0; i <= resolution; i++)
            {
                float angle = (float)i / (float)resolution * 2.0f * Mathf.PI;
                positions[i] = new Vector3(a * Mathf.Cos(angle), b * Mathf.Sin(angle), Bodies.Body.Z_LOCK);
                positions[i] = q * positions[i] + center3D;
            }

            return positions;
        }

        // See https://skotagiri.wordpress.com/2010/06/19/c-implementation-for-fitting-an-ellipse-for-a-set-of-points/
        // Updated to latest version of metanumerics at https://github.com/dcwuser/metanumerics/
        public static Parameters Fit(Vector3[] points)
        {
            int numPoints = points.Length;

            float[] coeff = new float[6];
            Parameters fit = new Parameters { };

            if (numPoints == 0) { Debug.LogWarning("Cannot fit empty data set."); return fit; }

            RectangularMatrix D1 = new RectangularMatrix(numPoints, 3);
            RectangularMatrix D2 = new RectangularMatrix(numPoints, 3);
            SquareMatrix S1 = new SquareMatrix(3);
            SquareMatrix S2 = new SquareMatrix(3);
            SquareMatrix S3 = new SquareMatrix(3);
            SquareMatrix T = new SquareMatrix(3);
            SquareMatrix M = new SquareMatrix(3);
            SquareMatrix C1 = new SquareMatrix(3);
            RectangularMatrix a1 = new RectangularMatrix(3, 1);
            RectangularMatrix a2 = new RectangularMatrix(3, 1);
            RectangularMatrix temp;

            C1[0, 0] = 0;
            C1[0, 1] = 0;
            C1[0, 2] = 0.5f;
            C1[1, 0] = 0;
            C1[1, 1] = -1;
            C1[1, 2] = 0;
            C1[2, 0] = 0.5f;
            C1[2, 1] = 0;
            C1[2, 2] = 0;

            //2 D1 = [x .ˆ 2, x .* y, y .ˆ 2]; % quadratic part of the design matrix
            //3 D2 = [x, y, ones(size(x))]; % linear part of the design matrix
            for (int xx = 0; xx < points.Length; xx++)
            {
                Vector3 p = points[xx];
                D1[xx, 0] = p.x * p.x;
                D1[xx, 1] = p.x * p.y;
                D1[xx, 2] = p.y * p.y;

                D2[xx, 0] = p.x;
                D2[xx, 1] = p.y;
                D2[xx, 2] = 1;
            }

            //4 S1 = D1’ * D1; % quadratic part of the scatter matrix
            temp = D1.Transpose * D1;
            for (int xx = 0; xx < 3; xx++)
                for (int yy = 0; yy < 3; yy++)
                    S1[xx, yy] = temp[xx, yy];

            //5 S2 = D1’ * D2; % combined part of the scatter matrix
            temp = D1.Transpose * D2;
            for (int xx = 0; xx < 3; xx++)
                for (int yy = 0; yy < 3; yy++)
                    S2[xx, yy] = temp[xx, yy];

            //6 S3 = D2’ * D2; % linear part of the scatter matrix
            temp = D2.Transpose * D2;
            for (int xx = 0; xx < 3; xx++)
                for (int yy = 0; yy < 3; yy++)
                    S3[xx, yy] = temp[xx, yy];

            //7 T = - inv(S3) * S2’; % for getting a2 from a1
            try
            {
                T = -1 * S3.Inverse() * S2.Transpose;
            }
            catch (DivideByZeroException e)
            {
                // TODO: Inverse (or subsequent) may throw DivideByZeroException
                // Is it too expensive to try-catch?
                // Attempt to avoid by calculating determinant via:
                //  * LUDecomposition.Determinant <-- still throws DivideByZeroException
                //  * QRDecomposition.Determinant <-- doesn't prevent Inverse->GausJordanInvert DivideByZeroException
                return fit;
            }

            //8 M = S1 + S2 * T; % reduced scatter matrix
            M = S1 + S2 * T;

            //9 M = [M(3, 🙂 ./ 2; - M(2, :); M(1, 🙂 ./ 2]; % premultiply by inv(C1)
            M = C1 * M;

            //10 [evec, eval] = eig(M); % solve eigensystem
            ComplexEigendecomposition eigenSystem = M.Eigendecomposition();

            //11 cond = 4 * evec(1, 🙂 .* evec(3, 🙂 - evec(2, 🙂 .ˆ 2; % evaluate a’Ca
            //12 a1 = evec(:, find(cond > 0)); % eigenvector for min. pos. eigenvalue
            for (int xx = 0; xx < eigenSystem.Dimension; xx++)
            {
                Complex[] vector = eigenSystem.eigenvectors[xx];
                Complex condition = 4 * vector[0] * vector[2] - vector[1] * vector[1];
                if (condition.Im == 0 && condition.Re > 0)
                {
                    // Solution is found
                    for (int yy = 0; yy < vector.Length; yy++)
                    {
                        a1[yy, 0] = vector[yy].Re;
                    }
                }
            }
            //13 a2 = T * a1; % ellipse coefficients
            a2 = T * a1;

            //14 a = [a1; a2]; % ellipse coefficients
            coeff[0] = (float)a1[0, 0];
            coeff[1] = (float)a1[1, 0];
            coeff[2] = (float)a1[2, 0];
            coeff[3] = (float)a2[0, 0];
            coeff[4] = (float)a2[1, 0];
            coeff[5] = (float)a2[2, 0];

            ComputeAxes(coeff, out fit.majorAxis, out fit.minorAxis, out fit.rotation);
            ComputeCenter(coeff, out fit.center);

            return fit;
        }

        // See https://en.wikipedia.org/wiki/Ellipse#General_ellipse
        #region Computation
        private static void ComputeAxes(float[] coefficients, out float majorAxis, out float minorAxis, out float rotation)
        {
            majorAxis = 0.0f;
            minorAxis = 0.0f;
            rotation = 0.0f;

            if (coefficients.Length != 6) { Debug.LogWarning("ComputeAxes: expecting 6 coefficients, got " + coefficients.Length + "."); return; }
            float A = coefficients[0];
            float B = coefficients[1];
            float C = coefficients[2];
            float D = coefficients[3];
            float E = coefficients[4];
            float F = coefficients[5];

            float d = Mathf.Pow(B, 2) - 4 * A * C;
            if (d == 0) { Debug.LogWarning("ComputeAxes: cannot divide by zero."); return; }

            float m = (A * Mathf.Pow(E, 2)) + (C * Mathf.Pow(D, 2)) - (B * D * E) + (F * (Mathf.Pow(B, 2) - 4 * A * C));
            float rem = Mathf.Sqrt(Mathf.Pow((A - C), 2) + Mathf.Pow(B, 2));

            // Set axes
            majorAxis = -1 * Mathf.Sqrt(2 * m * (A + C + rem)) / d;
            minorAxis = -1 * Mathf.Sqrt(2 * m * (A + C - rem)) / d;

            // Set rotation
            if (B == 0)
            {
                rotation = (A < C ? 0 : (90f * Mathf.Deg2Rad));
            }
            else
            {
                rotation = Mathf.Atan((C - A - rem) / B);
            }
        }

        private static void ComputeCenter(float[] coefficients, out Vector2 center)
        {
            center = Vector2.zero;

            if (coefficients.Length != 6) { Debug.LogWarning("ComputeAxes: expecting 6 coefficients, got " + coefficients.Length + "."); return; }
            float A = coefficients[0];
            float B = coefficients[1];
            float C = coefficients[2];
            float D = coefficients[3];
            float E = coefficients[4];
            float F = coefficients[5];

            float d = Mathf.Pow(B, 2) - 4 * A * C;
            if (d == 0) { Debug.LogWarning("ComputeAxes: cannot divide by zero."); return; }

            // Set center
            center.x = ((2 * C * D) - (B * E)) / d;
            center.y = ((2 * A * E) - (B * D)) / d;
        }
        #endregion
    }
}
