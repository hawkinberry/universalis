﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Universalis
{
    [RequireComponent(typeof(ParticleSystem))]
    [RequireComponent(typeof(Rigidbody2D))]
    public class Explosion : MonoBehaviour
    {
        ParticleSystem m_particleSystem;
        Rigidbody2D m_rigidBody;

        public float m_speedMultiplier = 0.2f;
        public float m_Dampen = 0.01f;
        public float m_minSpeed = 0.1f;

        private Vector3 m_origin;

        void Awake()
        {
            m_particleSystem = GetComponent<ParticleSystem>();
            m_rigidBody = GetComponent<Rigidbody2D>();
        }

        // Update is called once per frame
        void Update()
        {
            if (m_particleSystem && !m_particleSystem.IsAlive())
            {
                Destroy(gameObject);
            }
        }

        public void Initialize(Color color, Vector2 parentVelocity, Vector2 emissionVelocity, float aScale)
        {
            m_origin = transform.TransformPoint(transform.position);

            // Set emitter velocity
            m_rigidBody.velocity = parentVelocity * m_Dampen;

            // Set start color
            var particles = m_particleSystem.main;
            particles.startColor = color;
            particles.startSpeedMultiplier = m_speedMultiplier;

            // Set start size based on scale of object
            var startSize = particles.startSize.constant;
            startSize *= Mathf.Pow(aScale,10);

            var startSpeed = particles.startSpeed;
            startSpeed.constantMin = m_minSpeed;
            startSpeed.constantMax = emissionVelocity.magnitude * m_speedMultiplier;
            //Debug.Log(string.Format("Speed: ({0}, {1})",startSpeed.constantMin, startSpeed.constantMax));

            // Set initial direction of emission
            var shape = m_particleSystem.shape;
            shape.radius *= aScale;
            float rotation = -1 * Mathf.Atan2(emissionVelocity.y, emissionVelocity.x) * Mathf.Rad2Deg;
            shape.rotation = new Vector3(rotation, shape.rotation.y, shape.rotation.z);

            // Set emission count based on scale of object
            var emission = m_particleSystem.emission;
            var burst = emission.GetBurst(0);
            var count = burst.count;
            count.constant *= Mathf.Pow(aScale, 3);
            //Debug.Log("Generating " + count.constant + " particles");

        }

        void OnDrawGizmos()
        {
            Debug.Log("Hello world");
            if (Application.isEditor)
            {
                // Draw a yellow sphere at the transform's position
                Gizmos.color = Color.yellow;
                Vector3 drawPoint = transform.TransformPoint(transform.position);
                Gizmos.DrawSphere(drawPoint, 0.1f);
                Debug.Log("Drawing collision point at " + drawPoint);
            }
        }
    }
}
