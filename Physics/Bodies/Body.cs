﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Universalis.Bodies
{
    using UMath;

    public struct BodyState
    {
        public Vector3 position;
        public Vector3 velocity;
        public float scale;

        public override string ToString()
        {
            return string.Format("P: {0}; V: {1}; S: {2}", position, velocity, scale);
        }
    }

    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(CircleCollider2D))]
    public class Body : MonoBehaviour
    {
        public static event DeleteEventHandler OnDelete;

        public bool IsLocked { get; protected set; } = true;

        private int trackID;
        public int TrackID { get { return trackID; } set { trackID = value; } }

        public Vector2 initialVelocity = Vector2.zero;
        public bool startLocked = true;

        public bool colorOverride;
        public Color m_Color;

        protected static float KILL_DISTANCE = 100f;
        public static float Z_LOCK = 15f;

        public float m_minTrailWidth = 0.05f;

        public Ellipse.Parameters Trajectory;

        public SolarSystem homeSystem;

        #region Size
        public static float MIN_SIZE { get; } = 1f;
        public static float MAX_SIZE { get; } = 3f;
        public float COLLISION_WEIGHT_LIMIT { get; } = 0.65f;

        public virtual float Scale
        {
            get { return ApparentScale; }
            set { ApparentScale = value; }
        }

        protected float ApparentScale
        {
            get { return transform.localScale.x; }
            set { transform.localScale = Vector3.one * Mathf.Clamp(value, MIN_SIZE, MAX_SIZE); }
        }

        public float RadiusScale
        {
            get { return Scale / MIN_SIZE; }
        }
        #endregion

        protected Rigidbody2D m_rigidBody;
        protected CircleCollider2D m_circleCollider;
        protected SpriteRenderer m_spriteRenderer;

        #region State
        private BodyState m_State;
        public BodyState State
        {
            get { return m_State; }
        }

        public Vector3 Velocity
        {
            get { return m_rigidBody.velocity; }
        }
        #endregion

        public void Delete()
        {
            OnDelete?.Invoke(this);
            GameObject.Destroy(gameObject);
        }

        void Awake()
        {
            homeSystem = GetComponentInParent<SolarSystem>();
        }

        // Start is called before the first frame update
        protected virtual void Start()
        {
            IsLocked = startLocked;

            m_rigidBody = GetComponent<Rigidbody2D>();
            m_circleCollider = GetComponent<CircleCollider2D>();
            m_spriteRenderer = GetComponentInChildren<SpriteRenderer>();

            transform.position = new Vector3(transform.position.x, transform.position.y, Z_LOCK);
        }

        // Update is called once per frame
        public virtual void FixedUpdate()
        {
            m_State.position = this.transform.position;
            m_State.velocity = this.Velocity;
            m_State.scale = this.Scale;

            if (transform.position.magnitude > KILL_DISTANCE)
            {
                this.Delete();
            }
        }

        protected Vector2 CalculateForce(Body other)
        {
            return Utility.CalculateExpectedForce(this.State, other.State);
        }

        public Vector3 ComputeSurfaceNormal(Vector3 inPoint, out Vector3 pointOnSurface)
        {
            // Get local coordinates
            pointOnSurface = transform.InverseTransformPoint(inPoint);
            // Get direction to point
            Vector3 normal = pointOnSurface.normalized;

            // Push out from origin by radius of body (in world scale) in direction of normal
            pointOnSurface = new Vector3(transform.position.x + normal.x * m_circleCollider.radius * ApparentScale,
                                         transform.position.y + normal.y * m_circleCollider.radius * ApparentScale,
                                         transform.position.z);

            return normal;
        }

        public void AddForce(Vector2 force, ForceMode2D mode)
        {
            switch (mode)
            {
                case ForceMode2D.Impulse:
                    m_rigidBody.velocity = force;
                    break;
                case ForceMode2D.Force:
                default:
                    m_rigidBody.velocity += force * Executive.FixedDeltaTime;
                    break;
            }
        }

        public delegate void DeleteEventHandler(Body self);
    }
}
