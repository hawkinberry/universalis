﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Universalis
{
    namespace Bodies
    {
        [RequireComponent(typeof(SpriteRenderer))]
        [RequireComponent(typeof(TrailRenderer))]
        public class Planet : Body
        {
            TrailRenderer m_trail;

            public GameObject m_particleTemplate;

            private Transform m_sprite;

            // Start is called before the first frame update
            protected override void Start()
            {
                base.Start();

                m_sprite = transform.Find("Sprite");
                m_trail = transform.GetComponent<TrailRenderer>();

                // Select color
                if (colorOverride)
                {
                    m_spriteRenderer.color = m_Color;
                }
                else
                {
                    m_spriteRenderer.color = ColorGenerator.GetRandomColor();
                }

                m_trail.startColor = new Color(m_spriteRenderer.color.r, m_spriteRenderer.color.g, m_spriteRenderer.color.b, 1);
                m_trail.endColor = new Color(m_spriteRenderer.color.r, m_spriteRenderer.color.g, m_spriteRenderer.color.b, 0);

                m_circleCollider.enabled = false;

                if (false == startLocked)
                {
                    Launch(initialVelocity);
                    SolarSystem solarSystem = transform.parent.GetComponent<SolarSystem>();
                    if (solarSystem)
                    {
                        solarSystem.AddPlanet(this);
                    }
                }
            }

            // Update is called once per frame
            public override void FixedUpdate()
            {
                // Call this first, as it updates the body's state
                // that will be used to calculate forces
                base.FixedUpdate();

                Vector3 force = UpdateState();
                if (!IsLocked)
                {
                    this.AddForce(force, ForceMode2D.Force);
                }
            }

            private Vector2 UpdateState()
            {
                Vector2 netForce = Vector2.zero;

                Vector2 nearestStar = Vector2.right;
                float maxForce = Mathf.NegativeInfinity;

                foreach (Body p in homeSystem.System)
                {
                    // Do not calculate interactions with self
                    if (p != this)
                    {
                        Vector2 force = CalculateForce(p);
                        netForce += force;

                        if (force.magnitude > maxForce)
                        {
                            nearestStar = force.normalized;
                        }
                    }
                }

                m_sprite.rotation = Quaternion.Euler(0f, 0f, Vector2.SignedAngle(Vector2.right, nearestStar));
                return netForce;
            }

            public void Launch(Vector2 launchForce)
            {
                // Unlock
                IsLocked = false;
                m_circleCollider.enabled = true;

                // Set trail width
                float pixelRadius = m_minTrailWidth * RadiusScale;
                m_trail.startWidth = pixelRadius * 0.8f;
                m_trail.endWidth = pixelRadius * 0.1f;

                this.AddForce(launchForce, ForceMode2D.Impulse);
            }

            public void Destruct(Vector3 collisionPoint, Vector2 initialVelocity, Vector2 impact)
            {
                var particles = GameObject.Instantiate(m_particleTemplate, collisionPoint, Quaternion.identity, transform.parent);
                Explosion ex = particles.GetComponent<Explosion>();
                ex.Initialize(m_trail.startColor, initialVelocity, impact, RadiusScale);

                this.Delete();
            }

            void OnTriggerEnter2D(Collider2D c)
            {
                Body collider = c.gameObject.GetComponent<Body>();

                if (collider is Planet)
                {
                    // Only destroy this object if collider is significantly larger
                    // Problem: Dynamic rigid body of remaining collider is affected by collision.
                    //Debug.Log("Scale ratio: " + (collider.Scale / this.Scale));
                    if ((collider.Scale / this.Scale) < COLLISION_WEIGHT_LIMIT) { return; }

                    Vector3 collisionPoint = State.position;
                    Vector3 incidence = State.velocity;
                    Vector3 initialVelocity = incidence;

                    this.Destruct(collisionPoint, initialVelocity, incidence);

                    Audio.AudioManager.PlaySound("explosion");
                }
            }

        }
    }
}
