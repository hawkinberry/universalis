﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Universalis
{
    namespace Bodies
    {
        public class Star : Body
        {
            [SerializeField] private float Mass = 1.0f;

            public override float Scale
            {
                get { return base.Scale * Mass; }
                set { base.Scale = value / Mass; }
            }

            // Start is called before the first frame update
            protected override void Start()
            {
                base.Start();
            }

            // Star has dynamic rigid body, which detects collisions with kinematic bodies
            // Destroy kinematic bodies on collision
            void OnTriggerEnter2D(Collider2D c)
            {
                Body collider = c.gameObject.GetComponent<Body>();

                if (collider is Planet)
                {
                    Planet planet = collider as Planet;

                    Vector3 collisionPoint = planet.State.position;

                    Vector3 normal = this.ComputeSurfaceNormal(collisionPoint, out collisionPoint);
                    Vector3 incidence = normal * planet.State.velocity.magnitude;

                    planet.Destruct(collisionPoint, Vector2.zero, incidence);

                    Audio.AudioManager.PlaySound("explosion");
                }
            }
        }
    }
}