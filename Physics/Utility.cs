﻿using System.Collections.Generic;
using UnityEngine;

namespace Universalis.Bodies
{
    public class Utility : MonoBehaviour
    {
        #region Geometry
        public static float GetDistanceTo(Vector3 positionA, Vector3 positionB, out Vector3 direction)
        {
            Vector3 distance = positionB - positionA;
            direction = distance.normalized;

            return distance.magnitude;
        }
        #endregion

        #region Projection
        public static Vector3 CalculateExpectedForce(BodyState satellite, BodyState anchor)
        {
            Vector3 direction;
            float r = GetDistanceTo(satellite.position, anchor.position, out direction);

            float G = Constants.Gravity * (satellite.scale * anchor.scale) / Mathf.Pow(r, 2);
            Vector3 force = direction * G;

            return force;
        }

        public static Vector3 ProjectNextState(BodyState satellite, List<BodyState> anchors, float timeStep, out Vector3 nextVelocity)
        {
            Vector3 force = Vector3.zero;
            foreach (BodyState anchor in anchors)
            {
                force += CalculateExpectedForce(satellite, anchor);
            }

            Vector3 nextPosition = satellite.position + satellite.velocity * timeStep;
            nextVelocity = satellite.velocity + force * timeStep;

            return nextPosition;
        }
        #endregion

        // see https://answers.unity.com/questions/163337/velocity-before-collision.html
        #region Incidence
        public static Vector2 ComputeTotalImpulse(Collision2D collision)
        {
            Vector2 impulse = Vector2.zero;
            int contactCount = collision.contactCount;
            for (int i = 0; i < contactCount; i++)
            {
                var contact = collision.GetContact(0);
                impulse += contact.normal * contact.normalImpulse;
                impulse.x += contact.tangentImpulse * contact.normal.y;
                impulse.y -= contact.tangentImpulse * contact.normal.x;
            }
            return impulse;
        }

        public static Vector2 ComputeIncidentVelocity(Collision2D collision)
        {
            Vector2 otherVelocity;
            return ComputeIncidentVelocity(collision, out otherVelocity);
        }

        public static Vector2 ComputeIncidentVelocity(Collision2D collision, out Vector2 otherVelocity)
        {
            Vector2 impulse = ComputeTotalImpulse(collision);
            otherVelocity = Vector2.zero;
            var otherBody = collision.rigidbody;
            if (otherBody != null)
            {
                otherVelocity = otherBody.velocity;
                if (otherBody.isKinematic == false)
                    otherVelocity += impulse / otherBody.mass;
            }
            var myBody = collision.otherRigidbody;
            return myBody.velocity - impulse / myBody.mass;
        }
        #endregion
    }
}
