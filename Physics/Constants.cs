﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants : MonoBehaviour
{
    private static Constants m_Instance;

    [SerializeField] private float m_Gravity = 6.67408E-1f;
    [SerializeField] private float m_GravityMultiplier = 1f;

    public static float Gravity
    {
        get { return (m_Instance.m_Gravity * m_Instance.m_GravityMultiplier); }
    }

    public static float GravityMultiplier
    {
        get { return m_Instance.m_GravityMultiplier; }
        set { m_Instance.m_GravityMultiplier = value; }
    }

    void Awake()
    {
        m_Instance = this;
    }
}
