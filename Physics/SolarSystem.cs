﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Universalis
{
    using UMath;

    public class SolarSystem : MonoBehaviour
    {
        public GameObject SoundTrack;
        private Audio.GameTrack m_music;

        public bool ExperimentalMode = false;

        // System lists
        private List<Bodies.Body> sunSystem = new List<Bodies.Body>();
        private List<Bodies.Body> fullSystem = new List<Bodies.Body>();
        public List<Bodies.Body> System
        {
            get
            {
                return (ExperimentalMode ? fullSystem : sunSystem);
            }
        }

        private List<Bodies.Body> satellites = new List<Bodies.Body>();
        public List<Ellipse.Parameters> orbits = new List<Ellipse.Parameters>();

        void OnEnable()
        {
            Bodies.Body.OnDelete += RemovePlanet;
        }

        void OnDisable()
        {
            Bodies.Body.OnDelete -= RemovePlanet;
        }

        // Start is called before the first frame update
        void Start()
        {
            if (SoundTrack)
            {
                GameObject trackFile = GameObject.Instantiate(SoundTrack, transform);
                m_music = trackFile.GetComponent<Audio.GameTrack>();
            }

            if (Executive.currentMode == GameMode.SANDBOX)
            {
                m_music = null;
            }

            // Create star system
            foreach (Transform child in transform)
            {
                if (child.GetComponent<Bodies.Star>())
                {
                    Bodies.Body star = child.GetComponent<Bodies.Star>();
                    sunSystem.Add(star);
                    fullSystem.Add(star);
                }
            }

            //Debug.Log("System size: " + System.Count);
        }

        // Update is called once per frame
        void Update()
        {
            // Get rid of dead satellites
            PruneSystem();
        }

        
        public Bodies.Planet BuildPlanet(Bodies.Planet template, Vector3 initialPosition, float scale)
        {
            Bodies.Planet planet = Bodies.Planet.Instantiate(template, initialPosition, Quaternion.identity, transform);
            planet.Scale = scale;

            return planet;
        }

        public void AddPlanet(Bodies.Planet planet)
        {
            satellites.Add(planet);
            fullSystem.Add(planet);
            orbits.Add(planet.Trajectory);
        }

        public void RemovePlanet(Bodies.Body body)
        {
            if (body is Bodies.Planet)
            {
                Bodies.Planet planet = (Bodies.Planet)body;

                satellites.Remove(planet);
                fullSystem.Remove(planet);
                orbits.Remove(planet.Trajectory);
            }
        }

        // ! Returns track ID for planet
        public int LaunchPlanet(Bodies.Planet planet, Vector3 initialVelocity)
        {
            planet.Launch(initialVelocity);

            AddPlanet(planet);

            if (m_music)
            {
                return m_music.AddTrack();
            }

            return -1;
        }

        // Checks for "dead" bodies and removes from interaction lists
        private void PruneSystem()
        {
            // This might not be sufficient in race conditions
            satellites.RemoveAll(x => x == null);
            fullSystem.RemoveAll(x => x == null);
            sunSystem.RemoveAll(x => x == null);
        }

        public void Reset()
        {
            foreach (Bodies.Body b in satellites)
            {
                b.Delete();
            }

            satellites.Clear();
        }
    }
}
