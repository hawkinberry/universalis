﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorGenerator
{
    public static float c_COLOR_LIMIT = 0.3f;

    // Generate a random color within RGB limit
    public static float GetRandomColorValue(float opacity = 1f)
    {
        return Random.Range(c_COLOR_LIMIT, opacity);
    }

    public static Color GetRandomColor()
    {
        Color color = new Color(GetRandomColorValue(), GetRandomColorValue(), GetRandomColorValue());

        return color;
    }

}
