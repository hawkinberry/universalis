﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Universalis
{
    using UMath;

    public class Trajectory : MonoBehaviour
    {
        private const int STAR_LAYER = 9;

        [SerializeField] private float m_TimeToProject = 30; // seconds
        [SerializeField] private float m_baseTimeStep = 0.004f;
        private const float m_minTimeStep = 0.0001f;
        private const float m_cutoffDistance = 0.1f;
        private const float m_thresholdTime = 1.0f;
        [SerializeField] private AnimationCurve TrajectoryTimestepCurve;

        public List<Vector3> CalculatePoints(Bodies.BodyState initialState, int resolution, List<Bodies.Body> system)
        {
            // Initialize list of vertices for trajectory path
            List<Vector3> vertices = new List<Vector3>();
            vertices.Add(initialState.position);

            // Initialize first state vector
            float currentTime = m_baseTimeStep;
            Vector3 nextPoint = initialState.position;
            Vector3 nextVelocity = initialState.velocity;

            // lambda expression implementation ?
            List<Bodies.BodyState> anchors = new List<Bodies.BodyState>();
            foreach (Bodies.Body b in system)
            {
                anchors.Add(b.State);
            }

            // Initialize simulated satellite
            Bodies.BodyState satellite = initialState;

            // Loop until we reach desired projection time or we hit max number of vertices
            // i.e., we don't want to loop forever
            while (currentTime < m_TimeToProject && vertices.Count < resolution)
            {
                // Set current state vector
                satellite.position = nextPoint;
                satellite.velocity = nextVelocity;

                // Calculate distance between objects
                Vector3 direction; // won't use, but need to declare for "out" below
                float minDistance = Mathf.Infinity;
                foreach (Bodies.BodyState b in anchors)
                {
                    minDistance = Mathf.Min(minDistance, Bodies.Utility.GetDistanceTo(satellite.position, b.position, out direction));
                }

                // Scale timestep so that it is smaller at closer radii
                // Clamp it so it can't get too low at cost of performance
                float scaledTime = Mathf.Clamp(m_baseTimeStep * TrajectoryTimestepCurve.Evaluate(minDistance), m_minTimeStep, m_baseTimeStep);

                // Calculate next point in path
                nextPoint = Bodies.Utility.ProjectNextState(satellite, anchors, scaledTime, out nextVelocity);

                // Determine whether path intersects star
                Vector2 collision2D;
                if (Circle.DoesIntersectCircle(nextPoint, vertices[vertices.Count - 1], out collision2D, STAR_LAYER))
                {
                    // Terminate path at collision point
                    Vector3 intersectionPoint = new Vector3(collision2D.x, collision2D.y, nextPoint.z);
                    vertices.Add(intersectionPoint);
                    break;
                }

                // Else, add point in path
                // (limit the actual number of points in the line, for performance)
                if ((nextPoint - vertices[vertices.Count - 1]).magnitude >= m_cutoffDistance)
                {
                    vertices.Add(nextPoint);
                }
                currentTime += scaledTime;

                // Cut line if we loop back on the starting position
                if (currentTime > m_thresholdTime && (nextPoint - initialState.position).magnitude <= m_cutoffDistance)
                {
                    // close enough to start. End.
                    break;
                }
            }

            return vertices;
        }
    }
}
