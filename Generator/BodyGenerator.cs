﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Universalis
{
    using UMath;

    [RequireComponent(typeof(LineRenderer))]
    public class BodyGenerator : MonoBehaviour, IPointerDownHandler, IPointerUpHandler//, IBeginDragHandler, IEndDragHandler
    {
        static private BodyGenerator m_Instance;

        // Current launch
        private Bodies.Planet newPlanet = null;
        [Header("Trajectory Line")]
        LineRenderer trajectoryRender;
        public GameObject m_trajectory;
        private const int m_MaxVertices = 50000;

        [Header("System")]
        public Bodies.Planet bodyTemplate;
        public Camera worldCamera;
        public SolarSystem solarSystem;

        [Header("Power Settings")]
        public float m_powerMultiplier = 10f;
        //public float m_minPower;
        //public float m_maxPower;

        // Generation Settings
        public bool IsDisabled = false;
        public bool PersistTrajectory = false;
        private Vector3 startDrag;
        private Vector3 endDrag;
        private float setRadius = Bodies.Body.MIN_SIZE;
        private const float cRadiusIncrement = 2f;
        private const int LEFT_MOUSE_BUTTON = 0;
        private const int RIGHT_MOUSE_BUTTON = 1;

        void Awake()
        {
            m_Instance = this;
        }

        // Start is called before the first frame update
        void Start()
        {
            // Grab the camera with the "main camera" tag
            // May want to assign later
            worldCamera = Camera.main;

            trajectoryRender = transform.GetComponent<LineRenderer>();
        }

        void OnEnable()
        {
            Menus.GameOverlay.OnGamePause += Disable;
            Menus.GameOverlay.OnGameResume += Enable;
        }

        void OnDisable()
        {
            Menus.GameOverlay.OnGamePause -= Disable;
            Menus.GameOverlay.OnGameResume -= Enable;
        }

        private void Disable()
        {
            IsDisabled = true;
        }

        private void Enable()
        {
            IsDisabled = false;
        }

        // Update is called once per frame
        void Update()
        {
            if (!IsDisabled)
            {
                #region Mouse Click
                /*
                if (Input.GetMouseButtonDown(LEFT_MOUSE_BUTTON))
                {
                    //Debug.Log("Left mouse button down");
                    if (newPlanet == null)
                    {
                        setRadius = Bodies.Body.MIN_SIZE;
                    }

                    startDrag = GetCurrentMousePosition();

                    // Build new planet
                    newPlanet = solarSystem.BuildPlanet(bodyTemplate, startDrag, setRadius);

                    ResetTrajectory();
                }

                if (Input.GetMouseButtonUp(LEFT_MOUSE_BUTTON))
                {
                    //Debug.Log("Left mouse button up");
                    ClearTrajectory();

                    endDrag = GetCurrentMousePosition();

                    Vector2 direction = CalculateLaunchDirection(startDrag, endDrag);

                    if (newPlanet != null)
                    {
                        int trackID = solarSystem.LaunchPlanet(newPlanet, direction * m_powerMultiplier);
                        newPlanet.TrackID = trackID;
                    }

                    // Release control
                    newPlanet = null;
                }

                if (Input.GetMouseButtonDown(RIGHT_MOUSE_BUTTON))
                {
                    //Debug.Log("Right mouse button down");
                    ClearTrajectory();

                    if (newPlanet != null)
                    {
                        //Debug.Log("Destroying last created");
                        newPlanet.Delete();
                        newPlanet = null;
                    }
                }
                */
                #endregion

                #region Size Adjust
                if (Input.GetKey(KeyCode.UpArrow))
                {
                    setRadius += cRadiusIncrement * Time.deltaTime;
                    //Debug.Log("Increase Size -> " + setRadius);
                    UpdateRadius();
                }
                else if (Input.GetKey(KeyCode.DownArrow))
                {
                    setRadius -= cRadiusIncrement * Time.deltaTime;
                    //Debug.Log("Decrease Size -> " + setRadius);
                    UpdateRadius();
                }
                #endregion

                #region Update
                // Draw Trajectory if left moust button is held
                if (Input.GetMouseButton(LEFT_MOUSE_BUTTON))
                {
                    DrawTrajectory();
                }

                #endregion
            }
        }

        #region Pointer Handlers
        public void OnPointerDown(PointerEventData eventData)
        {
            if (!IsDisabled)
            {
                // Create new planet
                if (eventData.button == PointerEventData.InputButton.Left)
                {
                    if (newPlanet == null)
                    {
                        setRadius = Bodies.Body.MIN_SIZE;
                    }

                    startDrag = GetCurrentMousePosition();

                    // Build new planet
                    newPlanet = solarSystem.BuildPlanet(bodyTemplate, startDrag, setRadius);

                    ResetTrajectory();
                }
                // Destroy planet
                else if (eventData.button == PointerEventData.InputButton.Right)
                {
                    ClearTrajectory();

                    if (newPlanet != null)
                    {
                        newPlanet.Delete();
                        newPlanet = null;
                    }
                }
            }
        }

        private void LaunchPlanet()
        {
            Vector2 direction = CalculateLaunchDirection(startDrag, endDrag);
            Vector2 launchVelocity = direction * m_powerMultiplier;

            //Debug.Log("Launch " + startDrag + " at " + launchVelocity + " ups.");
            if (newPlanet != null)
            {
                Ellipse.Parameters form = Ellipse.Fit(GetLastTrajectory());
                newPlanet.Trajectory = form;

                int trackID = solarSystem.LaunchPlanet(newPlanet, launchVelocity);
                newPlanet.TrackID = trackID;
            }
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            if (!IsDisabled)
            {
                // Release planet
                if (eventData.button == PointerEventData.InputButton.Left)
                {
                    endDrag = GetCurrentMousePosition();
                    LaunchPlanet();

                    // Release control
                    if (!PersistTrajectory) { ClearTrajectory(); }
                    newPlanet = null;
                }
            }
        }
        #endregion

        public Vector2 CalculateLaunchDirection(Vector3 startPoint, Vector3 endPoint)
        {
            Vector2 direction = //new Vector2(Mathf.Clamp(startPoint.x - endPoint.x, m_minPower, m_maxPower),
                                //            Mathf.Clamp(startPoint.y - endPoint.y, m_minPower, m_maxPower));
                                new Vector2(startPoint.x - endPoint.x, startPoint.y - endPoint.y);

            return direction;
        }

        public Vector3[] GetLastTrajectory()
        {
            Vector3[] points = new Vector3[trajectoryRender.positionCount];
            trajectoryRender.GetPositions(points);

            return points;
        }

        public void ResetTrajectory()
        {
            trajectoryRender.positionCount = m_MaxVertices;
        }

        public void DrawTrajectory()
        {
            if (newPlanet == null)
            {
                return;
            }

            Vector2 launch = CalculateLaunchDirection(startDrag, GetCurrentMousePosition());

            Trajectory calc = m_trajectory.GetComponent<Trajectory>();
            Bodies.BodyState initialState = new Bodies.BodyState
            {
                position = startDrag,
                velocity = launch * m_powerMultiplier,
                scale = newPlanet.Scale
            };
            List<Vector3> vertices = calc.CalculatePoints(initialState, m_MaxVertices, solarSystem.System);

            // Load vertices into trajectory line renderer
            trajectoryRender.positionCount = vertices.Count;
            trajectoryRender.SetPositions(vertices.ToArray());
        }

        public void ClearTrajectory()
        {
            trajectoryRender.positionCount = 0;
        }

        private void UpdateRadius()
        {
            if (newPlanet != null)
            {
                newPlanet.Scale = setRadius;
                // Radius min-max clamps the value, so store update
                setRadius = newPlanet.Scale;
            }
        }

        private Vector3 GetCurrentMousePosition()
        {
            Vector3 clickPoint = worldCamera.ScreenToWorldPoint(Input.mousePosition);
            return new Vector3(clickPoint.x, clickPoint.y, Bodies.Body.Z_LOCK);
        }


    }
}
