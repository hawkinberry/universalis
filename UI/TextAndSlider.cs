﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Universalis.UI
{
    public class TextAndSlider : MonoBehaviour
    {
        [SerializeField] private Slider slider;
        [SerializeField] private InputField textField;

        public float Value { private set; get; }

        [Header("Settings")]
        public float minValue = 1.0f;
        public float maxValue = 100f;

        // Start is called before the first frame update
        void Start()
        {
            slider.minValue = minValue;
            slider.maxValue = maxValue;
        }

        public void Initialize(float val)
        {
            this.Value = val;
            UpdateSlider();
            UpdateInputField();
        }

        public void OnSliderChanged()
        {
            this.Value = slider.value;
            UpdateInputField();
        }

        public void OnTextChanged()
        {
            float tryParse;
            if (float.TryParse(textField.text, out tryParse))
            {
                if (tryParse >= minValue && tryParse <= maxValue)
                {
                    this.Value = tryParse;
                    UpdateSlider();
                }
                else
                {
                    UpdateInputField();
                }
            }
        }

        void UpdateSlider()
        {
            slider.value = this.Value;
        }

        void UpdateInputField()
        {
            textField.text = this.Value.ToString();
        }
    }
}
