﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using TMPro;

// Based on Unity UI Extensions - ScrollSnap by Simon Jackson
// https://bitbucket.org/UnityUIExtensions/unity-ui-extensions/src/release/
namespace Universalis.UI
{
    public class ScrollSelectNew : MonoBehaviour
    {
        // needed because of reversed behaviour of axis Y compared to X
        // (positions of children lower in children list in horizontal directions grows when in vertical it gets smaller)
        public enum ScrollDirection
        {
            Horizontal,
            Vertical
        }

        private RectTransform _rectTransform;

        private Transform _listContainerTransform;

        private int _pages;

        private int _startingPage = 0;

        private int _itemsVisibleAtOnce = 1;

        // anchor points to lerp to see child on certain indexes
        private Vector3[] _pageAnchorPositions;

        private Vector3 _lerpTarget;

        private bool _lerp;

        // item list related
        private float _listContainerMinPosition;

        private float _listContainerMaxPosition;

        private float _listContainerSize;

        private RectTransform _listContainerRectTransform;

        private Vector2 _listContainerCachedSize;

        private float _itemSize;

        private int _itemsCount = 0;

        public RectTransform _content;

        [Tooltip("Button to go to the next page. (optional)")]
        public Button NextButton;

        [Tooltip("Button to go to the previous page. (optional)")]
        public Button PrevButton;

        public ScrollDirection direction = ScrollDirection.Horizontal;

        // Use this for initialization
        void Start()
        {
            _lerp = false;

            _rectTransform = gameObject.GetComponent<RectTransform>();
            _listContainerTransform = _content;
            _listContainerRectTransform = _listContainerTransform.GetComponent<RectTransform>();

            //UpdateListItemsSize();
            UpdateListItemPositions();

            PageChanged(CurrentPage());

            if (NextButton)
            {
                NextButton.GetComponent<Button>().onClick.AddListener(() =>
                {
                    NextScreen();
                });
            }

            if (PrevButton)
            {
                PrevButton.GetComponent<Button>().onClick.AddListener(() =>
                {
                    PreviousScreen();
                });
            }
        }

        void LateUpdate()
        {
            //!UpdateListItemsSize();
            UpdateListItemPositions();

            if (_lerp)
            {
                // CHANGED : Time.deltaTime --> Time.unscaledDeltaTime
                // REASON  : Play animation even when time scale = 0
                _listContainerTransform.localPosition = Vector3.Lerp(_listContainerTransform.localPosition, _lerpTarget, 7.5f * Time.unscaledDeltaTime);

                if (Vector3.Distance(_listContainerTransform.localPosition, _lerpTarget) < 0.001f)
                {
                    _listContainerTransform.localPosition = _lerpTarget;
                    _lerp = false;
                }

                //change the info bullets at the bottom of the screen. Just for visual effect
                if (Vector3.Distance(_listContainerTransform.localPosition, _lerpTarget) < 10f)
                {
                    PageChanged(CurrentPage());
                }
            }
        }

        public void UpdateListItemPositions()
        {
            if (!_listContainerRectTransform.rect.size.Equals(_listContainerCachedSize))
            {
                // checking how many children of list are active
                int activeCount = 0;

                foreach (var tr in _listContainerTransform)
                {
                    if (((Transform)tr).gameObject.activeInHierarchy)
                    {
                        activeCount++;
                    }
                }

                // if anything changed since last check reinitialize anchors list
                _itemsCount = 0;
                Array.Resize(ref _pageAnchorPositions, activeCount);

                if (activeCount > 0)
                {
                    _pages = Mathf.Max(activeCount - _itemsVisibleAtOnce + 1, 1);

                    if (direction == ScrollDirection.Horizontal)
                    {
                        // looking for list spanning range min/max
                        //!_scroll_rect.horizontalNormalizedPosition = 0; //!
                        _listContainerMaxPosition = _listContainerTransform.localPosition.x;
                        //!_scroll_rect.horizontalNormalizedPosition = 1; //!
                        _listContainerMinPosition = _listContainerTransform.localPosition.x;

                        _listContainerSize = _listContainerMaxPosition - _listContainerMinPosition;

                        for (var i = 0; i < _pages; i++)
                        {
                            _pageAnchorPositions[i] = new Vector3(
                                _listContainerMaxPosition - _itemSize * i,
                                _listContainerTransform.localPosition.y,
                                _listContainerTransform.localPosition.z
                            );
                        }
                    }
                    else
                    {
                        //Debug.Log ("-------------looking for list spanning range----------------");
                        // looking for list spanning range
                        //!_scroll_rect.verticalNormalizedPosition = 1; //!
                        _listContainerMinPosition = _listContainerTransform.localPosition.y;
                        //!_scroll_rect.verticalNormalizedPosition = 0; //!
                        _listContainerMaxPosition = _listContainerTransform.localPosition.y;

                        _listContainerSize = _listContainerMaxPosition - _listContainerMinPosition;

                        for (var i = 0; i < _pages; i++)
                        {
                            _pageAnchorPositions[i] = new Vector3(
                                _listContainerTransform.localPosition.x,
                                _listContainerMinPosition + _itemSize * i,
                                _listContainerTransform.localPosition.z
                            );
                        }
                    }

                    //!UpdateScrollbar(LinkScrolbarSteps);
                    _startingPage = Mathf.Min(_startingPage, _pages);
                    //!ResetPage();
                }

                if (_itemsCount != activeCount)
                {
                    PageChanged(CurrentPage());
                }

                _itemsCount = activeCount;
                _listContainerCachedSize.Set(_listContainerRectTransform.rect.size.x, _listContainerRectTransform.rect.size.y);
            }

        }

        //Function for switching screens with buttons
        public void NextScreen()
        {
            //!UpdateListItemPositions();

            if (CurrentPage() < _pages - 1)
            {
                _lerp = true;
                _lerpTarget = _pageAnchorPositions[CurrentPage() + 1];

                PageChanged(CurrentPage() + 1);
            }
        }

        //Function for switching screens with buttons
        public void PreviousScreen()
        {
            //!UpdateListItemPositions();

            if (CurrentPage() > 0)
            {
                _lerp = true;
                _lerpTarget = _pageAnchorPositions[CurrentPage() - 1];

                PageChanged(CurrentPage() - 1);
            }
        }

        //returns the current screen that the is seeing
        public int CurrentPage()
        {
            float pos;

            if (direction == ScrollDirection.Horizontal)
            {
                pos = _listContainerMaxPosition - _listContainerTransform.localPosition.x;
                pos = Mathf.Clamp(pos, 0, _listContainerSize);
            }
            else
            {
                pos = _listContainerTransform.localPosition.y - _listContainerMinPosition;
                pos = Mathf.Clamp(pos, 0, _listContainerSize);
            }

            float page = pos / _itemSize;

            return Mathf.Clamp(Mathf.RoundToInt(page), 0, _pages);
        }

        public void ChangePage(int page)
        {
            if (0 <= page && page < _pages)
            {
                _lerp = true;

                _lerpTarget = _pageAnchorPositions[page];

                PageChanged(page);
            }
        }

        private void HideButton(GameObject button, bool setVisible)
        {
            // Set button interactable
            button.GetComponent<Button>().interactable = setVisible;
            
            // Set text transparency
            TextMeshProUGUI text = button.GetComponentInChildren<TextMeshProUGUI>();
            text.color = new Color(text.color.r, text.color.g, text.color.b,
                                   (setVisible ? 1f : 0f));
        }

        //changes the bullets on the bottom of the page - pagination
        private void PageChanged(int currentPage)
        {
            _startingPage = currentPage;

            if (NextButton)
            {
                bool show = currentPage < _pages - 1;
                HideButton(NextButton.gameObject, show);
            }

            if (PrevButton)
            {
                bool show = currentPage > 0;
                HideButton(PrevButton.gameObject, show);
            }
        }
    }
}
