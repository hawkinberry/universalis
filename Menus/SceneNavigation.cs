﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Universalis
{
    public class SceneNavigation : MonoBehaviour
    {
        public void OnMainMenuButtonPressed()
        {
            // In case we are coming here from a pause menu
            Time.timeScale = 1f;
            SceneManager.LoadScene("Title");
        }

        public void OnCreditsButtonPressed()
        {
            SceneManager.LoadScene("Credits");
            OnButtonPress();
        }

        public static void OnButtonPress()
        {
            Audio.AudioManager.PlaySound("button");
        }

        public void OnQuitButtonPressed()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#endif
            Application.Quit();
        }
    }
}
