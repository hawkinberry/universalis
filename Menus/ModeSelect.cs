﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

namespace Universalis
{
    public enum GameMode
    {
        CREATIVE,
        OBJECTIVE,
        SANDBOX
    };

    namespace Menus
    {
        public class ModeSelect : MonoBehaviour
        {
            public GameObject description;
            private TextMeshProUGUI descriptionText;

            public GameObject titleScreen;
            public GameObject modeScreen;
            public GameObject tutorialScreen;

            private string ObjectiveDescription = "Achieve precise, harmonic systems.\n\nNEW!";
            private string CreativeDescription = "Relax and create a work of art.";
            private string SandboxDescription = "Experiment with the fundamental laws of nature.\n\nNEW!";
            private string Unselected = "";

            public static string currentMode = "Creative";

            void Start()
            {
                descriptionText = description.GetComponent<TextMeshProUGUI>();
                SwapText(Unselected);

                titleScreen.SetActive(true);
                modeScreen.SetActive(false);
                tutorialScreen.SetActive(false);
            }

            public void OnModeButtonPressed(string mode)
            {
                currentMode = mode;
                SceneNavigation.OnButtonPress();
                ShowTutorial();
            }

            //public static IEnumerator StartGame()
            public static void StartGame()
            {
                AsyncOperation load;

                switch (currentMode)
                {
                    case "Objective":
                        Executive.currentMode = GameMode.OBJECTIVE;
                        //load = Objective.LevelNavigation.LoadLevelSelectionAsync(); // May cause scene script to load twice
                        Objective.LevelNavigation.LoadLevelSelection();
                        break;
                    case "Sandbox":
                        Executive.currentMode = GameMode.SANDBOX;
                        load = SceneManager.LoadSceneAsync("SandboxMode");
                        break;
                    case "Creative":
                    default:
                        Executive.currentMode = GameMode.CREATIVE;
                        load = SceneManager.LoadSceneAsync("CreativeMode");
                        break;
                }

                /*while (!load.isDone)
                {
                    yield return null;
                }*/
            }

            #region Descriptions
            public void OnObjectiveModeEnter()
            {
                SwapText(ObjectiveDescription);
            }

            public void OnCreativeModeEnter()
            {
                SwapText(CreativeDescription);
            }

            public void OnSandboxModeEnter()
            {
                SwapText(SandboxDescription);
            }

            public void OnButtonExit()
            {
                SwapText(Unselected);
            }

            private void SwapText(string text)
            {
                if (descriptionText == null)
                {
                    Debug.Log("Can't find TextMeshPro component");
                }

                if (descriptionText.text == null)
                {
                    Debug.Log("Can't find text");
                }
                descriptionText.text = text;
            }
            #endregion

            #region Screen Swap
            public void OnPlayButtonPressed()
            {
                SceneNavigation.OnButtonPress();
                TransitionToGameModeSelection();
            }

            public void OnBackButtonPressed()
            {
                SceneNavigation.OnButtonPress();
                TransitionToTitleScreen();
            }

            private void TransitionToGameModeSelection()
            {
                modeScreen.SetActive(true);
                titleScreen.SetActive(false);
            }

            private void TransitionToTitleScreen()
            {
                titleScreen.SetActive(true);
                modeScreen.SetActive(false);
            }
            #endregion

            #region Tutorial
            private void ShowTutorial()
            {
                tutorialScreen.SetActive(true);
            }
            #endregion
        }
    }
}
