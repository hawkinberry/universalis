﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Universalis
{
    public static class Executive
    {
        public static GameMode currentMode = GameMode.CREATIVE;

        public static float TimeScale = Time.timeScale;

        public static float FixedDeltaTime
        {
            get { return Time.fixedDeltaTime * TimeScale; }
        }

        public static void SetTimeScale(bool paused)
        {
            //TimeScale = (paused ? 0f : Time.timeScale);
            Time.timeScale = (paused ? 0f : 1f);
        }
    }
}
