﻿using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;
using UnityEngine.Events;

namespace Universalis
{
    public class Tutorial : MonoBehaviour
    {
        bool nextFrame = false;

        // Update is called once per frame
        void Update()
        {
            if (nextFrame)
            {
                //StartCoroutine(Menus.ModeSelect.StartGame());
                Menus.ModeSelect.StartGame();
            }

            if (Input.anyKeyDown)
            {
                TextMeshProUGUI bottomText = transform.Find("Continue").GetComponent<TextMeshProUGUI>();
                bottomText.text = "Loading ...";

                nextFrame = true;
            }
        }
    }
}
