﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

namespace Universalis.Menus
{
    using UI;

    public class SandboxConsole : MonoBehaviour
    {
        [Header("Panel Control")]
        public GameObject Panel;
        private float dockPosition;
        public GameObject ExpandButton;
        public GameObject CollapseButton;

        [Header("System")]
        private SolarSystem SolarSystem;
        private BodyGenerator Generator;
        public List<GameObject> systemOptions;

        [Header("Fields")]
        public TextAndSlider gravitySetting;
        private float m_gravityMultiplier;
        public TextAndSlider launchSetting;
        private float m_launchStrength;
        private bool m_Experimental = false;
        public Dropdown starSelector;

        // Start is called before the first frame update
        void Start()
        {
            SetSystemSize(0);

            dockPosition = Panel.transform.position.x;
            ClosePanel();

            m_gravityMultiplier = Constants.GravityMultiplier;
            gravitySetting.Initialize(m_gravityMultiplier);

            m_launchStrength = Generator.m_powerMultiplier;
            launchSetting.Initialize(m_launchStrength);
        }

        void OnEnable()
        {
            starSelector.onValueChanged.AddListener(
                delegate
                {
                    SetSystemSize(starSelector.value);
                });
        }

        void OnDisable()
        {
            starSelector.onValueChanged.RemoveAllListeners();
        }

        // Update is called once per frame
        void Update()
        {
            // Try and move this to an event handler
            m_gravityMultiplier = gravitySetting.Value;
            Constants.GravityMultiplier = m_gravityMultiplier;

            m_launchStrength = launchSetting.Value;
            Generator.m_powerMultiplier = m_launchStrength;
        }

        public void SetExperimental(bool value)
        {
            m_Experimental = value;
            SolarSystem.ExperimentalMode = value;
        }

        public void SetSystemSize(int size)
        {
            Debug.Log("Changing system size to " + (size + 1));
            SolarSystem oldSystem = GameObject.FindObjectOfType<SolarSystem>();
            if (oldSystem)
            {
                Destroy(oldSystem.gameObject);
            }

            GameObject newSystem = Instantiate(systemOptions[size], transform.parent);
            SolarSystem = newSystem.GetComponent<SolarSystem>();
            Generator = newSystem.GetComponentInChildren<BodyGenerator>();

            SetExperimental(m_Experimental);
        }

        public void OpenPanel(float duration = 0.0f)
        {
            ExpandButton.SetActive(false);
            CollapseButton.SetActive(true);

            Panel.LeanMoveX(dockPosition, duration);
        }

        public void ClosePanel(float duration = 0.0f)
        {
            RectTransform rect = Panel.GetComponent<RectTransform>();

            ExpandButton.SetActive(true);
            CollapseButton.SetActive(false);

            float moveTo = dockPosition - Panel.transform.TransformVector(rect.sizeDelta).x;
            Panel.LeanMoveX(moveTo, duration);
        }
    }
}
