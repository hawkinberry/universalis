﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Universalis
{
    namespace Menus
    {
        public class GameOverlay : MonoBehaviour
        {
            public static GamePauseEvent OnGamePause;
            public static GamePauseEvent OnGameResume;
            private bool m_GamePaused = false;

            [Header("Music Controls")]
            [SerializeField] private GameObject m_MusicGroup;
            [SerializeField] private GameObject m_SongSelect;
            [SerializeField] private GameObject m_MusicOnButton;
            [SerializeField] private GameObject m_MusicOffButton;

            [Header("Pause Menu")]
            [SerializeField] private Canvas m_PauseMenu;
            //[SerializeField] private Canvas m_SettingsMenu;

            private bool m_Enable = true;

            #region Initialize
            private void OnEnable()
            {
                // Register delegates
            }

            private void OnDisable()
            {
                // Deregister delegates
            }

            void Awake()
            {
                m_PauseMenu.enabled = m_GamePaused;
                // m_SettingsMenu.enabled = false;
            }

            void Start()
            {
                switch (Executive.currentMode)
                {
                    case GameMode.SANDBOX:
                        m_MusicGroup.SetActive(false);
                        break;
                    case GameMode.OBJECTIVE:
                        m_SongSelect.SetActive(false);
                        break;
                    case GameMode.CREATIVE:
                    default:
                        break;
                }
            }
            #endregion

            #region Pause
            void Update()
            {
                if (Input.GetButtonDown("Pause"))
                {
                    SetPause(!m_GamePaused);
                }
            }

            public void ResumeGame()
            {
                SetPause(false);
            }

            private void SetPause(bool isPaused)
            {
                m_GamePaused = isPaused;

                m_PauseMenu.enabled = isPaused;
                Executive.SetTimeScale(isPaused);
                Audio.AudioManager.SetPause(isPaused);

                if (isPaused)
                {
                    OnGamePause?.Invoke();
                }
                else
                {
                    OnGameResume?.Invoke();
                }
            }
            #endregion

            #region Settings
            public void OnSettings()
            {
                m_PauseMenu.enabled = false;
                //m_SettingsMenu.enabled = true;
            }

            public void OnSettingsBack()
            {
                m_PauseMenu.enabled = true;
                //m_SettingsMenu.enabled = false;
            }
            #endregion

            public void Disable()
            {
                m_Enable = false;
            }

            public void MusicOn()
            {
                Audio.AudioManager.SetMute(false);

                TextMeshProUGUI textON = m_MusicOnButton.GetComponentInChildren<TextMeshProUGUI>();
                TextMeshProUGUI textOFF = m_MusicOffButton.GetComponentInChildren<TextMeshProUGUI>();

                textON.color = Color.white;
                textOFF.color = Color.gray;
            }

            public void MusicOff()
            {
                Audio.AudioManager.SetMute(true);

                TextMeshProUGUI textON = m_MusicOnButton.GetComponentInChildren<TextMeshProUGUI>();
                TextMeshProUGUI textOFF = m_MusicOffButton.GetComponentInChildren<TextMeshProUGUI>();

                textON.color = Color.gray;
                textOFF.color = Color.white;
            }

            public void OnSoundButtonPressed()
            {
                bool muted = Audio.AudioManager.ToggleMute();

                //m_MuteOnButton.SetActive(muted);
                //m_MuteOffButton.SetActive(!muted);
            }

            public void TrackBack()
            {
                Debug.Log("Previous Track");
            }

            public void TrackAdvance()
            {
                Debug.Log("Next Track");
            }

            public delegate void GamePauseEvent();
        }
    }
}
